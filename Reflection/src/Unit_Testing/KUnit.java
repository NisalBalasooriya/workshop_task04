package Unit_Testing;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



public class KUnit {
	
	private static List<String> checks;
	  private static int checksMade = 0;
	  private static int passedChecks = 0;
	  private static int failedChecks = 0;
	  private static Logger logger = LogManager.getLogger(KUnit.class);
	  
	  
	  //Reporting //
	  private static void addToReport(String txt) {
		    if (checks == null) {
		      checks = new LinkedList<String>();
		    }
		    checks.add(String.format("%04d: %s", checksMade++, txt));
		  }
	  
	  //Simple assertions //
	  public static void checkEquals(double value1, double value2) {
		  try {
			  if (value1 == value2) {
			      addToReport(String.format("  %.2f == %.2f", value1, value2));
			      passedChecks++;
			    } else {
			      addToReport(String.format("* %.2f == %.2f", value1, value2));
			      failedChecks++;
			    }   
		  }catch (Exception e){
			  addToReport(String.format("! Exception in checkEquals: %s", e.getMessage()));
			  failedChecks++;	  
		  }	  
	  }
	  
	  
	  public static void checkNotEquals(double value1, double value2) {
		  try {
			  if (value1 != value2) {
			      addToReport(String.format("  %.2f != %.2f", value1, value2));
			      passedChecks++;
			    } else {
			      addToReport(String.format("* %.2f != %.2f", value1, value2));
			      failedChecks++;
			    }
		  }catch (Exception e) {
			  addToReport(String.format("! Exception in checkNotEquals: %s", e.getMessage()));
			  failedChecks++;  
		  }	  
	  }
	  
	  //Launcher //
	  public static void runChecks(Class<?> testClass) {
		  Method[] methods = testClass.getDeclaredMethods();

		  for (Method method : methods) {
		    if (method.getName().startsWith("check")) {
		      try {
		        Object instance = testClass.getDeclaredConstructor().newInstance();
		        method.setAccessible(true);
		        addToReport(String.format("Running check: %s", method.getName()));
		        method.invoke(instance);  
		      } catch (Exception e) {
		        addToReport(String.format("! %s - Exception: %s", method.getName(), e.getMessage()));
		        failedChecks++;
		      }
		    }
		  }
		}
	  
	  //Complex assertions //
	  
	  //Check Instance variable
	  public static void checkInstanceVariable(Object object, String variableName, Object expectedValue) {
		  try {
		    Field field = object.getClass().getDeclaredField(variableName);
		    field.setAccessible(true);
		    Object actualValue = field.get(object);
		    Class<?> fieldType = field.getType();

		    if (Objects.equals(actualValue, expectedValue)) {
		      addToReport(String.format("  %s.%s (%s) == %s",
		          object.getClass().getSimpleName(), variableName, fieldType.getSimpleName(), expectedValue));
		      passedChecks++;
		    } else {
		      addToReport(String.format("* %s.%s (%s) == %s",
		          object.getClass().getSimpleName(), variableName, fieldType.getSimpleName(), expectedValue));
		      failedChecks++;
		    }
		  } catch (Exception e) {
		    addToReport(String.format("! Exception in checkInstanceVariable: %s", e.getMessage()));
		    failedChecks++;
		  }
		}
	  
	  //Check Method Return Value
	  public static void checkMethodReturnValue(Object object, String methodName, Object expectedValue) {
		    try {
		      Method method = object.getClass().getDeclaredMethod(methodName);
		      method.setAccessible(true);
		      Object actualValue = method.invoke(object);

		      if (Objects.equals(actualValue, expectedValue)) {
		        addToReport(String.format("  %s.%s() == %s", object.getClass().getSimpleName(), methodName, expectedValue));
		        passedChecks++;
		      } else {
		        addToReport(String.format("* %s.%s() == %s", object.getClass().getSimpleName(), methodName, expectedValue));
		        failedChecks++;
		      }
		    } catch (Exception e) {
		      addToReport(String.format("! Exception in checkMethodReturnValue: %s", e.getMessage()));
		      failedChecks++;
		    }
		  }
	  
	  //Reporting //
	  public static void report() {
		    try {
		        logger.info("");

		        logger.info(String.format("%d checks passed", passedChecks));
		        logger.info(String.format("%d checks failed", failedChecks));

		        for (String check : checks) {
		            logger.info(check);
		        }
		    } catch (Exception e) {
		        logger.error("! Exception: " + e.getMessage());
		    }
		}
}
