package Unit_Testing;
import static Unit_Testing.KUnit.checkEquals;
import static Unit_Testing.KUnit.checkNotEquals;
import static Unit_Testing.KUnit.report;
import static Unit_Testing.KUnit.runChecks;
import static Unit_Testing.KUnit.checkMethodReturnValue;
import static Unit_Testing.KUnit.checkInstanceVariable;


public class TestAddSubtract {
	
	 void checkConstructorAndAccess(){
		 AddSubtract as = new AddSubtract(3.4, 4.5);
		 checkEquals(as.getA(), 4.5);
		 checkEquals(as.getB(), 4.5);
		 checkNotEquals(as.getB(), 4.5);    
		 checkNotEquals(as.getB(), 5.5); 
		 checkInstanceVariable(as, "a", 4.5);
		 checkInstanceVariable(as, "b", 4.5);
	 }
	 
	 void checkAddNumbers() {
		 AddSubtract as = new AddSubtract(3.5, 4.5);
		 as.addNumbers(3.5, 4.5);
		 checkEquals(as.getA(), 8.00);
		 checkMethodReturnValue(as, "getA", 12.25);
		 
	 }
	 
	 public static void main(String[] args) {
		 TestAddSubtract ts = new TestAddSubtract();
		 ts.checkConstructorAndAccess();
		 ts.checkAddNumbers();
		 runChecks(TestAddSubtract.class);
		 report();
		 
		 
	 }

}
