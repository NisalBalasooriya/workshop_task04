package Experiments;

public class Reflection03 {
	
	public static void main(String[] args) {
		AddSubtract operation = new AddSubtract();
		System.out.println("class = " + operation.getClass());
		System.out.println("class name = " + operation.getClass().getName());
	}
	

}
