package Experiments;

public class AddSubtract {
	
	public double a = 3.14;
	private double b = 2.71;
	
	public AddSubtract() {
	}
	
	public AddSubtract(double a, double b) {
		this.a = a;
		this.b = b;
	}
	
	public double addNumbers(double a, double b) {
		return a + b;
	}
	
	private double subtractNumbers(double a, double b) {
		return a - b;
	}
	
	public double getA() {
	    return a;
	}
	
	private void setA(double a) {
	    this.a = a;
	  }

	  public double getB() {
	    return b;
	  }

	  public void setB(double b) {
	    this.b = b;
	  }

	  public String toString() {
	    return String.format("(a:%f, b:%f)", a, b);
	  }
}
