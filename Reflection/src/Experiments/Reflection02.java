package Experiments;

public class Reflection02 {
	
	public static void main(String[] args) {
	    AddSubtract operation = new AddSubtract();
	    operation.addNumbers(4.5 ,4.7);
	    //operation.subtractNumbers(4.7, 4,.5); // if you uncomment this you will get a compiler error
	    double a = operation.a;
	    // double b = operation.b; // if you uncomment this you will get a compiler error
	    System.out.println("Numbers=" + operation);
	    
	  }

}
