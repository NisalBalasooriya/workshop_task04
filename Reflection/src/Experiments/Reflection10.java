package Experiments;

import java.lang.reflect.Method;

public class Reflection10 {
	public static void main(String[] args) throws Exception {
	    AddSubtract operation = new AddSubtract();
	    Method m = operation.getClass().getDeclaredMethod("setA", double.class);
	    m.setAccessible(true);
	    m.invoke(operation, 76);
	    System.out.println(operation);
	}

}
